import Grid from './Grid'
import IData from './interfaces/IData'
import MyExactSolution from './MyExactSolution'
import ImprovedEuler from './ImprovedEuler'
import Euler from './Euler'
import RungeKutta from './RungeKutta'
import LocalError from './LocalError'

export default class Data implements IData{

    private _x0: number
    private _x_end: number
    private _N: number
    private _h: number
    private _y0: number
    private _solutionGrid?: Grid
    private _errGrid? : Grid
    private _totalErrorsGrid? : Grid
    private _computeConstant: (x0 : number, y0 : number) => number
    private _c: number
    private _x: number[]
    private _f: (x: number, y: number) => number
    private _F: (x: number, c: number) => number
    private _n0: number
    private _n_end: number

    private calculateStep (x0: number, x_end: number, N: number) : number{
        return (x_end - x0) / N
    }

    private calculateX(x0: number, x_end: number, h: number){
        const x = []
        for (let xi = x0; xi < x_end + h; xi += h){
            x.push(xi)
        }
        return x
    }

    constructor(x0, x_end, N, y0, f, F, computeConstant, n0?, n_end?){
        this._x0 = x0
        this._x_end = x_end
        this._N = N
        this._h = this.calculateStep(this._x0, this._x_end, this._N)
        this._y0 = y0
        this._f = f
        this._F = F
        this._computeConstant = computeConstant
        this._c = computeConstant(x0, y0)
        this._x = this.calculateX(this._x0, this._x_end, this._h)
        this._n0 = n0
        this._n_end = n_end
    }

    private updateSolutionGrid(){
        this._solutionGrid.dataSet.map(set => {
            if (set.isApproximation()) {
                set.updateData(this._y0, this._x, this._h)
            }else{
                set.updateData(this._x, this._c)
            }
        })
    }

    private updateErrorGrid(){
        let yExact = undefined
        for (let i = 0; i < this._solutionGrid.dataSet.length; i++){
            if (!this._solutionGrid.dataSet[i].isApproximation()){
                yExact = this._solutionGrid.dataSet[i]
                break
            }
        }
        for (let i = 0; i < this._solutionGrid.dataSet.length; i++){
            if (this._solutionGrid.dataSet[i].isApproximation()){
                this._errGrid.dataSet[i].updateData(yExact.y, this._solutionGrid.dataSet[i].y, this._x)
            }
        }
    }

    private updateTotalErrorsGrid(){
        this._totalErrorsGrid.dataSet = this.calcTotalErrors()
    }

    recalculateAll(){
        this._h = this.calculateStep(this._x0, this._x_end, this._N)
        this._c = this._computeConstant(this._x0, this._y0)
        this._x = this.calculateX(this._x0, this._x_end, this._h)

        if (this._solutionGrid){
            this.updateSolutionGrid()
            this._solutionGrid.draw()
        }
        if (this._errGrid) {
            this.updateErrorGrid()
            this._errGrid.draw()
        }
        if (this._totalErrorsGrid){
            this.updateTotalErrorsGrid()
            this._totalErrorsGrid.draw()
        }
    }

    calcTotalErrors(){
        const step: number[] = []
        for (let i: number = this._n0; i<=this._n_end; i++){ step.push(i) }
        const maxErrors = [
            {x: step, y: []},
            {x: step, y: []},
            {x: step, y: []}
        ]
        for (let i = this._n0; i<= this._n_end; i++){
            const h = this.calculateStep(this._x0, this._x_end, i)
            const x = this.calculateX(this._x0, this._x_end, h)
            const tempSolutions = [
                new MyExactSolution(x, this._c, this._F),
                new ImprovedEuler(this._y0, x, h, this._f),
                new Euler(this._y0, x, h, this._f),
                new RungeKutta(this._y0, x, h, this._f)
            ]
            const tempErrors = [
                new LocalError(tempSolutions[0].y, tempSolutions[1].y, x),
                new LocalError(tempSolutions[0].y, tempSolutions[2].y, x),
                new LocalError(tempSolutions[0].y, tempSolutions[3].y, x)
            ]
            maxErrors.forEach((obj, idx) => {
                obj.y.push(tempErrors[idx].maxLTE(i))
            })
        }
        return maxErrors
    }

    set n0(val: number){
        val >= 0 && val < this._n_end?
            this._n0 = val:
            alert('value is not in domain of valid values')
        this.recalculateAll()
    }
    set n_end(val: number){
        val > this._n0 && val > this._n0?
            this._n_end = val:
            alert('value is not in domain of valid values')
        this.recalculateAll()
    }
    set x0(val: number){
        val / 3 + 1 / 6 + this._computeConstant(val, this._y0) * Math.exp(2*val) >= 0 && val < this._x_end?
            this._x0 = val:
            alert ('value is not in domain of valid values')

        this.recalculateAll()
    }
    set x_end(val: number){
        val > this._x0?
            this._x_end = val:
            alert('x_end should be bigger than x0')
        this.recalculateAll()
    }
    set y0(val: number){
        val >= 0?
            this._y0 = val:
            alert('y0 should be non-negative')
        this.recalculateAll()
    }
    set N(val: number){
        val > 0?
            this._N = val:
            alert('N should be positive')
        this.recalculateAll()
    }
    set solutionGrid(grid: Grid){
        this._solutionGrid = grid
    }
    set errGrid(grid: Grid){
        this._errGrid = grid
    }
    set totalErrorsGrid(grid: Grid){
        this._totalErrorsGrid = grid
    }
    get x0(): number{
        return this._x0
    }
    get x_end(): number{
        return this._x_end
    }
    get h(): number{
        return this._h
    }
    get y0(): number{
        return this._y0
    }
    get solutionGrid(): Grid{
        return this._solutionGrid
    }
    get c(): number{
        return this._c
    }
    get x(): number[]{
        return this._x
    }
    get f(): (x: number, y: number) => number{
        return this._f
    }
    get F(): (x: number, c: number) => number{
        return this._F
    }
    get N(): number{
        return this._N
    }

}