import IExactSolution from './interfaces/IExactSolution'

export default class MyExactSolution implements IExactSolution{
    private _x: number[];
    private _y: number[];
    private _c: number
    private _F: (x: number, c: number) => number;

    constructor(x: Array<number>, c:number , F: (x: number, c: number) => number){
        this._x = x
        this._c = c
        this._F = F
        this.compute()
    }

    compute(){
        this._y = []
        this._x.map(val => {
            this._y.push(this._F(val, this._c))
        })
    }

    updateData(x: number[], c: number){
        this._x = x
        this._c = c
        this.compute()
    }

    isApproximation(): boolean {
        return false
    }

    get x(): number[]{
        return this._x
    }
    get y(): number[]{
        return this._y
    }
}