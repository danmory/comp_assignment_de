import {Chart} from 'chart.js'

export default class Grid{

    constructor(dataSet, labels, colors, $el){
        this.dataSet = dataSet
        this.labels = labels
        this.colors = colors
        this._$el = $el
        this.chart = undefined
    }

    info(){
        const arr = []
        this.dataSet.map((set, idx) => {
            arr.push({
                borderColor: this.colors[idx],
                lineTension: 0,
                data: set.y,
                label: this.labels[idx],
                fill: false
            })
        })
        console.log(arr)
        return arr
    }

    draw(){
        if (this.chart) {
            this.chart.destroy()
        }

        this.chart = new Chart(this._$el, {
            type: 'line',
            data: {
                labels: this.dataSet[0].x.map(String),
                datasets: this.info()
            }
        })
    }

}

