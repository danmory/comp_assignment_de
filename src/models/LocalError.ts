export default class LocalError{
    private _yExact: number[]
    private _yApproximate: number[]
    private _x: number[]
    private _LTE: number[]

    constructor(yExact: number[], yApproximate: number[], x: number[]) {
        this.updateData(yExact, yApproximate, x)
    }

    updateData(yExact, yApproximate, x) {
        this._yExact = yExact
        this._yApproximate = yApproximate
        this._x = x
        this._LTE = this.compute()
    }

    compute(){
        const LTE: number[] = []
        for (let i = 0; i < this._yExact.length; i++){
            LTE.push(Math.abs(this._yExact[i] - this._yApproximate[i]))
        }
        return LTE
    }

    maxLTE(endID: number): number{
        return Math.abs(this._yExact[endID] - this._yApproximate[endID])
    }

    get y(){
        return this._LTE
    }
    get x(){
        return this._x
    }
}