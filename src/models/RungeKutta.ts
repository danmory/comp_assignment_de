import INumericalMethod from './interfaces/INumericalMethod'

export default class RungeKutta implements INumericalMethod{

    private _y0: number
    private _x: number[]
    private  _h: number
    private _f: (x: number, y: number) => number
    private _y: number[]

    constructor(y0: number, x: number[], h: number, f: (x: number, y: number) => number) {
        this._f = f
        this.updateData(y0, x, h)
    }

    updateData(y0: number, x: number[], h: number){
        this._y0 = y0
        this._x = x
        this._h = h
        this.compute()
    }

    compute(){
        this._y = [this._y0]
        for (let i:number = 1; i < this._x.length; i++){
            const k1 = this._f(this._x[i-1], this._y[i-1])
            const k2 = this._f(this._x[i-1] + this._h/2, this._y[i-1] + this._h * k1 / 2)
            const k3 = this._f(this._x[i-1] + this._h/2, this._y[i-1] + this._h * k2 / 2)
            const k4 = this._f(this._x[i-1] + this._h, this._y[i-1] + this._h * k3)
            this._y.push(this._y[i-1] + this._h * (k1 + 2*k2 + 2*k3 + k4) / 6)
        }
    }

    isApproximation(){
        return true
    }

    get x(): number[]{
        return this._x
    }
    get y(): number[]{
        return this._y
    }
}