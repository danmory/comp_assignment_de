export default interface IExactSolution{
    compute(): void
    isApproximation(): boolean
    updateData(x: number[], c:number): void
    x: number[]
    y: number[]
}