export default interface INumericalMethod{
    updateData(y0: number, x: number[], h: number, f: (x: number, y: number) => number)
    compute(): void
    isApproximation(): boolean
    x: number[]
    y: number[]
}