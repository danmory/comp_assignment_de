import './styles/styles.css'
import Grid from './models/Grid'
import ImprovedEuler from './models/ImprovedEuler'
import Euler from './models/Euler'
import RungeKutta from './models/RungeKutta'
import {f, F, computeConstant, addListeners } from './utils'
import Data from './models/Data'
import MyExactSolution from './models/MyExactSolution'
import LocalError from './models/LocalError'

const x0_input = document.querySelector('#x0')
const n0_input = document.querySelector('#n0')
const y0_input = document.querySelector('#y0')
const N_input = document.querySelector('#N')
const x_end_input = document.querySelector('#x_end')
const n_end_input = document.querySelector('#n_end')

const data = new Data(1, 6, 6, 2, f, F, computeConstant, 10, 100)
addListeners(data,
      [x0_input, y0_input, N_input, x_end_input, n0_input, n_end_input]
)

const ie = new ImprovedEuler(data.y0, data.x, data.h, data.f)
const euler = new Euler(data.y0, data.x, data.h, data.f)
const rk = new RungeKutta(data.y0, data.x, data.h, data.f)
const exact = new MyExactSolution(data.x, data.c, F )
const lte_ie = new LocalError(exact.y, ie.y, data.x )
const lte_euler = new LocalError(exact.y, euler.y, data.x )
const lte_rk = new LocalError(exact.y, rk.y, data.x )

const solutionGrid = new Grid(
    [ie, euler, rk, exact],
    ['Improved Euler', 'Euler', 'Runge Kutta', 'Exact Solution'],
    ['red', 'blue', 'green', 'black'],
    document.querySelector('.solution-graph')
)
const errGrid = new Grid(
    [lte_ie, lte_euler, lte_rk],
    ['Improved Euler error', 'Euler error', 'Runge Kutta error'],
    ['red', 'blue', 'green'],
    document.querySelector('.local-error-graph')
)
const totalErrorsGrid = new Grid(
    data.calcTotalErrors(),
    ['Improved Euler', 'Euler', 'Runge-Kutta'],
    ['red', 'blue', 'green'],
    document.querySelector('.total-error-graph')
)

data.solutionGrid = solutionGrid
data.errGrid = errGrid
data.totalErrorsGrid = totalErrorsGrid
solutionGrid.draw()
errGrid.draw()
totalErrorsGrid.draw()
