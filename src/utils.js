export const F = (x, c) => {
    return Math.sqrt(Math.pow((2 * x + 1 + c * Math.exp(2 * x)), 3)) / (6 * Math.sqrt(6))
}

export const f = (x, y) => 3*y - x*Math.cbrt(y)

export const computeConstant = (x0, y0) => (Math.cbrt(Math.pow(6 * Math.sqrt(6) * y0, 2)) - 2 * x0 - 1 ) / Math.exp(2*x0)

const setFloatValue = val => val? parseFloat(val) : 0

export function addListeners(data, inputs) {
    inputs.forEach(input => {
        input.addEventListener('change', (event) => {
            data[event.target.name] = setFloatValue(event.target.value)
        })
    })
}

